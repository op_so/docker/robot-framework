*** Settings ***
Documentation       Sanity tests of Docker image
...
...                 A test suite to validate the Docker image.

Library             Collections
Library             Process
Library             RequestsLibrary
Library             SeleniumLibrary

Test Tags           image


*** Variables ***
${TESTS_DIR}                tests
${GITLAB_API_VERSION}       v4
${GITLAB_USERNAME}          fxs
${GITLAB_NAME}              jfxs
${GRID_URL}                 http://selenium:4444/wd/hub


*** Test Cases ***
Robot Framework: [--version] option
    [Documentation]    Robot Framework with --version option
    [Tags]    core
    ${result} =    When Run Process    robot    --version
    Then Should Be Equal As Integers    ${result.rc}    251
    And Should Contain    ${result.stdout}    Robot Framework

Requests library
    [Documentation]    Requests library test
    [Tags]    request
    ${resp} =    When GET
    ...    https://gitlab.com/api/${GITLAB_API_VERSION}/users
    ...    params=username=${GITLAB_USERNAME}
    ...    expected_status=200
    ${user} =    Then Get From List    ${resp.json()}    0
    And Dictionary Should Contain Item    ${user}    name    ${GITLAB_NAME}

Selenium library
    [Documentation]    Selenium library test
    [Tags]    selenium
    Given Open Browser
    ...    https://testpages.herokuapp.com/styled/basic-web-page-test.html
    ...    firefox
    ...    remote_url=${GRID_URL}
    And Set Screenshot Directory    EMBED
    Then Wait Until Page Contains    Basic Web Page Example
    And Capture Page Screenshot
    [Teardown]    Close All Browsers

Import library
    [Documentation]    Import fakerlibrary test
    [Tags]    fakerlibrary
    When Import Library    FakerLibrary
    ${words} =    FakerLibrary.Words
    Then Log    words: ${words}
