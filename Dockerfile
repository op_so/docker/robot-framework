# hadolint ignore=DL3007
FROM python:3-alpine

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker
ENV HOME /.robot

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="robot-framework" \
    org.opencontainers.image.description="A lightweight Docker image to run Robot Framework tests" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/robot-framework" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/robot-framework" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL3018,DL3013
RUN apk --no-cache add \
        curl \
        go-task \
    && apk --no-cache add --virtual \
        .build-deps \
        build-base \
        libffi-dev \
        python3-dev \
    && pip3 install --no-cache-dir \
        setuptools \
        robotframework==${VERSION} \
        robotframework-requests \
        robotframework-robocop \
        robotframework-tidy \
        selenium==4.* \
        robotframework-seleniumlibrary \
    && apk --no-cache del .build-deps \
# hadolint ignore=DL3059
    && ln -s /usr/bin/go-task /usr/bin/task \
    && addgroup nobody root \
    && mkdir -p /tests /reports /.robot \
    && chgrp -R 0 /reports /.robot \
    && chmod -R g=u /etc/passwd /reports /.robot

WORKDIR /tests

COPY .Taskfile-robot.yml /robot.yml
COPY files/uid-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["uid-entrypoint.sh"]

USER 10010
CMD ["robot", "--version"]
